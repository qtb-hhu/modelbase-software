{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# modelbase tutorial\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import annotations\n",
    "\n",
    "from importlib.metadata import version\n",
    "from typing import Iterable\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from assets.models import upper_glycolysis, phase_plane_model\n",
    "from assets.utils import print_as_table\n",
    "from modelbase.ode import DerivedStoichiometry, Model, Simulator, mca\n",
    "from scipy.integrate import solve_ivp\n",
    "\n",
    "for pkg in (\"modelbase\",):\n",
    "    print(f\"{pkg:<10} {version(pkg)}\")\n",
    "\n",
    "\n",
    "def kelvin_from_celsius(t: float) -> float:\n",
    "    return t + 273.15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation\n",
    "\n",
    "Let's say you want to model the following chemical network\n",
    "\n",
    "$$ \\Large \\varnothing \\xrightarrow{v_0} S \\xrightarrow{v_1} P \\xrightarrow{v_2} \\varnothing $$\n",
    "\n",
    "\n",
    "To translate that into a set of differential equations we first write out the stoichiometric matrix $N$\n",
    "\n",
    "|   | $v_0$ | $v_1$ | $v_2$ |\n",
    "|---|------:|------:|------:|\n",
    "| S |     1 |    -1 |     0 |\n",
    "| P |     0 |     1 |    -1 |\n",
    "\n",
    "which translates into\n",
    "\n",
    "$$\\begin{align*}\n",
    "\\frac{dS}{dt} &= v_0 - v_1     \\\\\n",
    "\\frac{dP}{dt} &= v_1 - v_2 \\\\\n",
    "\\end{align*}\n",
    "$$\n",
    "\n",
    "and then choose rate equations for each rate to get the flux vector $v$\n",
    "\n",
    "$$\\begin{align*}\n",
    "    v_0 &= k_0 \\\\\n",
    "    v_1 &= k_1 * S \\\\\n",
    "    v_2 &= k_2 * P \\\\\n",
    "\\end{align*}$$\n",
    "\n",
    "<!-- $$v = \\left\\{ \n",
    "    \\begin{align*}\n",
    "    & k_0 \\\\\n",
    "    & k_1 * S \\\\\n",
    "    & k_2 * P \\\\\n",
    "    \\end{align*} \n",
    "\\right.$$ -->\n",
    "\n",
    "Then the system of ODEs is given by $\\frac{d\\text{Model}}{dt} = Nv$, which gives us\n",
    "\n",
    "\n",
    "$$\\begin{align*}\n",
    "\\frac{dS}{dt} &= k_0 - k_1 * S     \\\\\n",
    "\\frac{dP}{dt} &= k_1 * S - k_2 * P \\\\\n",
    "\\end{align*}$$\n",
    "\n",
    "\n",
    "\n",
    "Since the stoichiometric matrix is sparse, writing it out completely would do a lot of unnecessary computations.\n",
    "\n",
    "The easiest way to do that would be to create your model as a function that returns the derivatives `dSdt` and `dPdt` and then to integrate that using an ODE solver."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def model(\n",
    "    _: float, y: Iterable[float], k_in: float, k1: float, k_out: float\n",
    ") -> Iterable[float]:\n",
    "    # Unpack state vector\n",
    "    s, p = y\n",
    "\n",
    "    # Flux vector\n",
    "    v_in = k_in\n",
    "    v1 = k1 * s\n",
    "    v_out = k_out * p\n",
    "\n",
    "    # Stoichiometric matrix\n",
    "    dSdt = v_in - v1\n",
    "    dPdt = v1 - v_out\n",
    "    return dSdt, dPdt\n",
    "\n",
    "\n",
    "t_span = (0, 10)\n",
    "t_eval = np.linspace(*t_span, 100)\n",
    "integration = solve_ivp(\n",
    "    model,\n",
    "    t_span=t_span,\n",
    "    t_eval=t_eval,\n",
    "    y0=(0, 0),\n",
    "    args=(1, 1, 1),\n",
    ")\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(integration.t, integration.y.T)\n",
    "ax.set_xlabel(\"time / a.u.\")\n",
    "ax.set_ylabel(\"Concentration / a.u.\")\n",
    "ax.legend([\"S\", \"P\"], loc=\"upper left\", bbox_to_anchor=(1.01, 1), borderaxespad=0)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating your first model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While this works well for small models, changing the model or creating model-variants requires you to re-write that function, probably introducing errors along the way.  \n",
    "What you want is to be able to have a *modular* interface to building your models.  \n",
    "`modelbase` supplies you with the `Model` object, which you can use to iteratively build your models.\n",
    "\n",
    "Let's begin by defining rate functions.  \n",
    "Note that these should be **general** and **re-usable** whenever possible, to make your model clear to people reading it.  \n",
    "Try to give these functions names that are meaningful to your audience, e.g. a rate function `k * s` could be named **proportional** or **mass-action**.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def constant(k: float) -> float:\n",
    "    return k\n",
    "\n",
    "\n",
    "def proportional(k: float, s: float) -> float:\n",
    "    return k * s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create the model by\n",
    "\n",
    "- adding compounds (variables) and parameters (constants).  \n",
    "- adding the three reactions using `add_reaction_from_args`\n",
    "\n",
    "The method `add_reaction_from_args` has the following arguments:\n",
    "\n",
    "- `rate_name`: The name you want to give your rate. Has to be **unique** \n",
    "- `function`: The python function you defined above\n",
    "- `stoichiometry`: A dictionary defining which compounds will be *consumed* (negative value) or *produced* (positive value) by the reaction\n",
    "- `args`: A list of all arguments that will be passed to the python function. The order is the order in which the function will receive the arguments!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def linear_chain_2cpds() -> Model:\n",
    "    m = Model()\n",
    "    m.add_compounds([\"S\", \"P\"])\n",
    "    m.add_parameters({\"k_in\": 1, \"k_1\": 1, \"k_out\": 1})\n",
    "    m.add_reaction_from_args(\n",
    "        rate_name=\"v0\",\n",
    "        function=constant,\n",
    "        stoichiometry={\"S\": 1},\n",
    "        args=[\"k_in\"],\n",
    "    )\n",
    "    m.add_reaction_from_args(\n",
    "        rate_name=\"v1\",\n",
    "        function=proportional,\n",
    "        stoichiometry={\"S\": -1, \"P\": 1},\n",
    "        args=[\"k_1\", \"S\"],\n",
    "    )\n",
    "    m.add_reaction_from_args(\n",
    "        rate_name=\"v2\",\n",
    "        function=proportional,\n",
    "        stoichiometry={\"P\": -1},\n",
    "        args=[\"k_out\", \"P\"],\n",
    "    )\n",
    "    return m"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Note that we defined our model in a function.  \n",
    "> This isn't strictly necessary, but is highly recommended, as it avoids problems of hidden state.  \n",
    "\n",
    "\n",
    "\n",
    "\n",
    "There are some legacy variants of building the stoichiometric matrix, which you might encounter.  \n",
    "\n",
    "- `Model.add_reaction`\n",
    "- `Model.add_reaction_from_ratelaw`\n",
    "- `Model.add_rate`\n",
    "- `Model.add_rates`\n",
    "- `Model.add_stoichiometry`\n",
    "- `Model.add_stoichiometry_by_compound`\n",
    "- `Model.add_stoichiometries`\n",
    "- `Model.add_stoichiometries_by_compounds`\n",
    "\n",
    "Do note however, that `add_reaction_from_args` is the preferred way.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simulating the model\n",
    "\n",
    "Create a `Simulator` object by \n",
    "\n",
    "- passing the model into it\n",
    "- initialising the simulator with a dictionary containing the **initial conditions**\n",
    "- simulate the model until time point `t_end = 10`\n",
    "- plot the result\n",
    "\n",
    "If we just want to plot a single result, we can make use of the fact that `initialise` and `simulate_and` both return the `Simulator` object and we thus make an easy-to-read and beautiful pipeline."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = (\n",
    "    Simulator(linear_chain_2cpds())\n",
    "    .initialise({\"S\": 0, \"P\": 0})\n",
    "    .simulate_and(t_end=10)\n",
    "    .plot(\n",
    "        xlabel=\"time / a.u.\",\n",
    "        ylabel=\"concentration / a.u.\",\n",
    "        title=\"Linear chain\",\n",
    "        figure_kwargs={\"figsize\": (5, 3.5)},\n",
    "    )\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you require multiple plots I recommend to assign the result of `simulate_and` to a variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = (\n",
    "    Simulator(linear_chain_2cpds())\n",
    "    .initialise({\"S\": 0, \"P\": 0})\n",
    "    .simulate_and(t_end=10)  # stop assignment here\n",
    ")\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(7, 3))\n",
    "s.plot(\n",
    "    xlabel=\"time / a.u.\",\n",
    "    ylabel=\"concentration / a.u.\",\n",
    "    title=\"Linear chain\",\n",
    "    ax=ax1,\n",
    ")\n",
    "s.plot_phase_plane(\n",
    "    cpd1=\"S\",\n",
    "    cpd2=\"P\",\n",
    "    title=\"Linear chain phase plane\",\n",
    "    ax=ax2,\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a lot more plot functions to be explored.  \n",
    "Explore them - usually they will be sufficient for most of your needs. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print_as_table([i for i in dir(s) if i.startswith(\"plot\")])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simulate a steady-state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t_ss, y_ss = (\n",
    "    Simulator(linear_chain_2cpds())\n",
    "    .initialise({\"S\": 0, \"P\": 0})\n",
    "    .simulate_to_steady_state()\n",
    ")\n",
    "print(y_ss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Parameter scans\n",
    "\n",
    "Very often you want to systematically investigate the **steady-state concentrations** depending on different values of a parameter.  \n",
    "For this you can use the `Simulator.parameter_scan` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_scan = (\n",
    "    Simulator(linear_chain_2cpds())\n",
    "    .initialise({\"S\": 0, \"P\": 0})\n",
    "    .parameter_scan(parameter_name=\"k_in\", parameter_values=np.linspace(0, 10, 11))\n",
    ")\n",
    "y_scan"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are also interested in the **steady-state fluxes**, you can use the `Simulator.parameter_scan_with_fluxes` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_scan, v_scan = (\n",
    "    Simulator(linear_chain_2cpds())\n",
    "    .initialise({\"S\": 0, \"P\": 0})\n",
    "    .parameter_scan_with_fluxes(\n",
    "        parameter_name=\"k_in\", parameter_values=np.linspace(0, 10, 11)\n",
    "    )\n",
    ")\n",
    "v_scan"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Phase plane analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 1, figsize=(6, 4))\n",
    "\n",
    "s = Simulator(phase_plane_model()).initialise({\"s1\": 10, \"s2\": 0})\n",
    "s.plot_trajectories(\n",
    "    cpd1=\"s1\",\n",
    "    cpd2=\"s2\",\n",
    "    y0={\"s1\": 0, \"s2\": 0},\n",
    "    cpd1_bounds=(0, 2),\n",
    "    cpd2_bounds=(0, 2),\n",
    "    n=20,\n",
    "    ax=ax,\n",
    ")\n",
    "for s1 in np.linspace(0, 1, 4):\n",
    "    for s2 in np.linspace(0, 2, 4):\n",
    "        (\n",
    "            s.initialise({\"s1\": s1, \"s2\": s2})\n",
    "            .simulate_and(t_end=1.5, steps=1000)\n",
    "            .plot_phase_plane(\"s1\", \"s2\", ax=ax)\n",
    "        )\n",
    "fig.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Metabolic control analysis\n",
    "\n",
    "`modelbase` provides routines to calculate two important measures from metabolic control analysis\n",
    "\n",
    "- elasticities, which are measured at any given state of the system\n",
    "- response coefficients, which are measured at steady-state"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Elasticities\n",
    "\n",
    "You can get the elasticities of the compounds / substrates via `mca.get_compound_elasticities_df` and the ones for the parameters using `mca.get_parameter_elasticities_df`.\n",
    "\n",
    "There are routines for different data structures as well, but usually `DataFrame`s are the preferred choice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mca.get_compound_elasticities_df(\n",
    "    upper_glycolysis(),\n",
    "    compounds=[\"GLC\", \"F6P\"],\n",
    "    y={\n",
    "        \"GLC\": 0.3,\n",
    "        \"G6P\": 0.4,\n",
    "        \"F6P\": 0.5,\n",
    "        \"FBP\": 0.6,\n",
    "        \"ATP\": 0.4,\n",
    "        \"ADP\": 0.6,\n",
    "    },\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mca.get_parameter_elasticities_df(\n",
    "    upper_glycolysis(),\n",
    "    parameters=[\"k1\", \"k2\"],\n",
    "    y={\n",
    "        \"GLC\": 0.3,\n",
    "        \"G6P\": 0.4,\n",
    "        \"F6P\": 0.5,\n",
    "        \"FBP\": 0.6,\n",
    "        \"ATP\": 0.4,\n",
    "        \"ADP\": 0.6,\n",
    "    },\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Response coefficients\n",
    "\n",
    "You can get the control and flux response coefficients using `mca.get_response_coefficients_df`.  \n",
    "\n",
    "There are routines for different data structures as well, but usually `DataFrame`s are the preferred choice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "crc, frc = mca.get_response_coefficients_df(\n",
    "    upper_glycolysis(),\n",
    "    parameters=[\"k1\", \"k2\", \"k3\", \"k4\", \"k5\", \"k6\", \"k7\"],\n",
    "    y={\n",
    "        \"GLC\": 0,\n",
    "        \"G6P\": 0,\n",
    "        \"F6P\": 0,\n",
    "        \"FBP\": 0,\n",
    "        \"ATP\": 0.5,\n",
    "        \"ADP\": 0.5,\n",
    "    },\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting coefficients\n",
    "\n",
    "You can plot the response coefficients using `mca.plot_coefficient_heatmap`.  \n",
    "This function will also work with the elasticities.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = mca.plot_coefficient_heatmap(\n",
    "    crc,\n",
    "    title=\"Concentration response coefficient\",\n",
    "    annotate=False,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Quite often you might want to plot multiple response coefficients.  \n",
    "For example, that might be both concentration and flux response coefficients.\n",
    "\n",
    "You can do this using `mca.plot_multiple`, which can plot an arbitrary amount of heatmaps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = mca.plot_multiple(\n",
    "    [crc, frc],\n",
    "    titles=[\"Concentration response coefficients\", \"Flux response coefficients\"],\n",
    "    annotate=False,\n",
    "    figsize=(8, 4),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deeper dive\n",
    "\n",
    "`modelbase` contains a number of fundamental building blocks\n",
    "\n",
    "- parameters (constants)\n",
    "- compounds (variables)\n",
    "- reactions, which consist of\n",
    "  - rates\n",
    "  - stoichiometries\n",
    "\n",
    "Frequently, one wants to build building blocks that depend on those fundamental blocks.  \n",
    "For example, the product of the gas constant `R` with the temperature `T` occurs quite often in thermodynamics.\n",
    "Thus, one might just want to define `R` and `T` and use a derived item `RT = R * T`.\n",
    "\n",
    "`modelbase` offers a number of those derived building blocks, which differ in *when* they are calculated.\n",
    "\n",
    "- derived *parameters* are calculated from other parameters and thus **before** the model is integrated\n",
    "- derived *compounds* (also called algebraic modules) are calculated from other variables and thus **before** any rates\n",
    "- derived stoichiometries are calculated from other variables and thus **before** any rates\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Derived parameters\n",
    "\n",
    "Derived parameters depend **only** on other parameters.  \n",
    "They are defined using `Model.add_derived_parameter(name, function, parameters)`.  \n",
    "They are calculated at every parameter change.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def derived_parameter_example() -> Model:\n",
    "    m = Model()\n",
    "    m.add_parameter(\"temperature_celsius\", 25.0)\n",
    "    m.add_derived_parameter(\n",
    "        \"temperature_kelvin\",\n",
    "        kelvin_from_celsius,\n",
    "        parameters=[\"temperature_celsius\"],\n",
    "    )\n",
    "    return m\n",
    "\n",
    "derived_parameter_example().parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Derived compounds / algebraic modules\n",
    "\n",
    "Algebraic modules can depend on parameters and compounds (dynamic variables).  \n",
    "They are calculated every time the model is called.  \n",
    "\n",
    "Algebraic modules can define **multiple** derived compounds.  \n",
    "As frequently only a single derived compound is required, there exists the shortcut function `add_derived_compound`, which doesn't require explicitly setting the derived compounds.  \n",
    "\n",
    "Thus, their function signatures are as follows\n",
    "\n",
    "`Model.add_derived_compound(name, function, args)`  \n",
    "`Model.add_algebraic_module_from_args(name, function, derived_compounds, args)`  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def derived_compound_example() -> Model:\n",
    "    m = Model()\n",
    "    m.add_compound(\"temperature_celsius\")\n",
    "    m.add_derived_compound(\n",
    "        \"temperature_kelvin\",\n",
    "        kelvin_from_celsius,\n",
    "        args=[\"temperature_celsius\"],\n",
    "    )\n",
    "    return m\n",
    "\n",
    "\n",
    "derived_compound_example().get_derived_variables({\"temperature_celsius\": 25.0})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def distribute(s: float) -> tuple[float, float]:\n",
    "    return s / 3, s * 2 / 3\n",
    "\n",
    "\n",
    "def algebraic_module_example() -> Model:\n",
    "    m = Model()\n",
    "    m.add_compound(\"a\")\n",
    "    m.add_algebraic_module_from_args(\n",
    "        \"distribute\",\n",
    "        distribute,\n",
    "        derived_compounds=[\"a1\", \"a2\"],\n",
    "        args=[\"a\"],\n",
    "    )\n",
    "    return m\n",
    "\n",
    "\n",
    "algebraic_module_example().get_derived_variables({\"a\": 1.0})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Derived stoichiometries\n",
    "\n",
    "Like derived parameters, derived stoichiometries **only** depend on parameters.  \n",
    "They are defined along with their respective reactions using the additional\n",
    "`derived_stoichiometry` argument.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def diffusion(inside: float, outside: float, k: float) -> float:\n",
    "    return k * (outside - inside)\n",
    "\n",
    "\n",
    "def div(x: float, y: float) -> float:\n",
    "    return x / y\n",
    "\n",
    "\n",
    "def derived_stoichiometries_example() -> Model:\n",
    "    m = Model()\n",
    "    m.add_compounds([\"x_in\"])\n",
    "    m.add_parameters(\n",
    "        {\n",
    "            \"x_base_stoichiometry\": 1.0,\n",
    "            \"cell_size\": 1.0,\n",
    "            \"x_out\": 1.0,\n",
    "            \"k_diffusion\": 1.0,\n",
    "        }\n",
    "    )\n",
    "    m.add_reaction_from_args(\n",
    "        \"rxn\",\n",
    "        diffusion,\n",
    "        stoichiometry={},\n",
    "        derived_stoichiometry={\n",
    "            \"x_in\": DerivedStoichiometry(div, [\"x_base_stoichiometry\", \"cell_size\"])\n",
    "        },\n",
    "        args=[\"x_in\", \"x_out\", \"k_diffusion\"],\n",
    "    )\n",
    "    return m\n",
    "\n",
    "\n",
    "print(derived_stoichiometries_example().stoichiometries)\n",
    "print(\n",
    "    derived_stoichiometries_example()\n",
    "    .update_parameter(\"cell_size\", 2.0)  # update cell size\n",
    "    .stoichiometries\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Modularisation and composition\n",
    "\n",
    "The best way to build larger models is to compose them from smaller building blocks.  \n",
    "That makes testing and analysing the building blocks a lot easier.  \n",
    "For larger scale modularisation I recommend our [qtbmodels](https://gitlab.com/marvin.vanaalst/qtbmodels) package which builds on top of modelbase.  \n",
    "\n",
    "For small scale modularisation however, you can also quickly build model variants by expanding and modifying an existing model.  \n",
    "To make it easy for others to see exactly what you changed, I recommend building them as functions that take other model functions as arguments.\n",
    "\n",
    "There are two ways of doing this.  \n",
    "\n",
    "- implicitly, creating the old model inside the new one\n",
    "- explicitly, passing the old model as an argument to the new one\n",
    "\n",
    "```python\n",
    "def create_model_v2() -> Model:\n",
    "    m = create_model_v1()\n",
    "    m.add_parameter(\"k_1_rev\", 0.5)\n",
    "    m.add_reaction_from_args(\n",
    "        \"v1_rev\", proportional, {\"P\": -1, \"S\": 1}, [\"k_1_rev\", \"P\"]\n",
    "    )\n",
    "    return m\n",
    "\n",
    "def create_model_v2(m: Model) -> Model:\n",
    "    m.add_parameter(\"k_1_rev\", 0.5)\n",
    "    m.add_reaction_from_args(\n",
    "        \"v1_rev\", proportional, {\"P\": -1, \"S\": 1}, [\"k_1_rev\", \"P\"]\n",
    "    )\n",
    "    return m\n",
    "```\n",
    "\n",
    "I would generally recommed the latter approach, as it allows you to re-use the changes you are making in the second model variant.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def create_model_v1() -> Model:\n",
    "    m = Model()\n",
    "    m.add_compounds([\"S\", \"P\"])\n",
    "    m.add_parameters({\"k_in\": 1, \"k_1\": 1, \"k_out\": 1})\n",
    "    m.add_reaction_from_args(\"v0\", constant, {\"S\": 1}, [\"k_in\"])\n",
    "    m.add_reaction_from_args(\"v1\", proportional, {\"S\": -1, \"P\": 1}, [\"k_1\", \"S\"])\n",
    "    m.add_reaction_from_args(\"v2\", proportional, {\"P\": -1}, [\"k_out\", \"P\"])\n",
    "    return m\n",
    "\n",
    "\n",
    "def create_model_v2(m: Model) -> Model:\n",
    "    m.add_parameter(\"k_1_rev\", 0.5)\n",
    "    m.add_reaction_from_args(\n",
    "        \"v1_rev\", proportional, {\"P\": -1, \"S\": 1}, [\"k_1_rev\", \"P\"]\n",
    "    )\n",
    "    return m\n",
    "\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(7, 3), sharey=True)\n",
    "_ = (\n",
    "    Simulator(create_model_v1())\n",
    "    .initialise({\"S\": 0, \"P\": 0})\n",
    "    .simulate_and(t_end=10)\n",
    "    .plot(\n",
    "        xlabel=\"time / a.u.\",\n",
    "        ylabel=\"concentration / a.u.\",\n",
    "        title=\"Linear chain v1\",\n",
    "        ax=ax1,\n",
    "    )\n",
    ")\n",
    "_ = (\n",
    "    Simulator(create_model_v2(create_model_v1()))\n",
    "    .initialise({\"S\": 0, \"P\": 0})\n",
    "    .simulate_and(t_end=10)\n",
    "    .plot(\n",
    "        xlabel=\"time / a.u.\",\n",
    "        ylabel=\"concentration / a.u.\",\n",
    "        title=\"Linear chain v2\",\n",
    "        ax=ax2,\n",
    "    )\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introspection & bug fixing\n",
    "\n",
    "Not all the times model creation goes as smoothly as in the example.  \n",
    "To allow you to quickly find an error, modelbase contains introspection methods, to quickly find out where things went wrong.  \n",
    "We can check the constructed system, by taking a look at the stoichiometric matrix, the fluxes and the right hand side.\n",
    "\n",
    "Let's start with the stoichiometric matrix, which modelbase returns as a `dataframe`.  \n",
    "Here you can see which reaction affects which compound by how much.  \n",
    "If a number here is unexpected, you know that you need to change the respective `stoichiometry` dictionary.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "linear_chain_2cpds().get_stoichiometric_df()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next step is to check the fluxes of the system.  \n",
    "This is especially useful to see whether the direction of all reactions is as expected or to check if a reaction is overly large or small"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "linear_chain_2cpds().get_fluxes_df({\"S\": 1.0, \"P\": 0.5})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You *can* also check the right hand side of the differential equation.  \n",
    "Since that is just the stoichiometric matrix multiplied by the fluxes this doesn't give any new information, but it is an easy way to check if all reactions are going in the correct direction.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "linear_chain_2cpds().get_right_hand_side({\"S\": 1.0, \"P\": 0.5})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some bugs only appear after a certain amount of time.  \n",
    "The simulator object allows checking the concentrations over time (results) and fluxes over time.  \n",
    "\n",
    "If you need to access the concentrations over time for something other than plotting, you can do that using `.get_results_df()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    Simulator(linear_chain_2cpds())\n",
    "    .initialise({\"S\": 0, \"P\": 0})\n",
    "    .simulate_and(t_end=10)\n",
    "    .get_results_df()\n",
    "    .head(5)  # type: ignore\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A just like with the model you can get the fluxes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    Simulator(linear_chain_2cpds())\n",
    "    .initialise({\"S\": 0, \"P\": 0})\n",
    "    .simulate_and(t_end=10)\n",
    "    .get_fluxes_df()\n",
    "    .head(5)  # type: ignore\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sharp edges"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### lambda functions\n",
    "\n",
    "Several important bits of modelbase require that all parts of the model can be [pickled](https://docs.python.org/3/library/pickle.html).  \n",
    "This is especially true for parallelisation, for which Python requires this.  \n",
    "\n",
    "Since `lambda` functions cannot be pickled, it is highly recommended to avoid using them wherever possible.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Misc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Explicit time dependency\n",
    "\n",
    "If a rate is explicily dependent on time, `modelbase` gives access to that using the `time` argument in any algebraic module or reaction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def time_dependency() -> Model:\n",
    "    m = Model()\n",
    "    m.add_compound(\"x\")\n",
    "    m.add_reaction_from_args(\n",
    "        \"v1\",\n",
    "        proportional,\n",
    "        {\"x\": -1},\n",
    "        [\"time\", \"x\"],\n",
    "    )\n",
    "    return m\n",
    "\n",
    "\n",
    "fig, ax = (\n",
    "    Simulator(time_dependency())\n",
    "    .initialise({\"x\": 1})\n",
    "    .simulate_and(t_end=10)\n",
    "    .plot(\n",
    "        xlabel=\"time / a.u.\",\n",
    "        ylabel=\"amount / a.u.\",\n",
    "        title=\"Time-dependent reaction\",\n",
    "        figure_kwargs={\"figsize\": (5, 3)},\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Readouts\n",
    "\n",
    "Readouts are a small performance enhancement that essentially function like derived compounds, but cannot be used by anything else in the model.  \n",
    "\n",
    "`Model().add_readout(name, function, args)`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generating source code\n",
    "\n",
    "`modelbase` models can generate their own source code using `Model().generate_model_source_code()`.  \n",
    "\n",
    "The source code generated is in canonical form instead of trying to reproduce the exact code used to generate the model.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(linear_chain_2cpds().generate_model_source_code())"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "3ade19fe28da7284566173cdd2419aac45095a5c0c1dc22df26ab34e245f1a65"
  },
  "kernelspec": {
   "display_name": "Python 3.9.12 ('py39')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "384px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
