Changelog
=========


1.57.18 (2024-12-29)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.17 (2024-12-22)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.16 (2024-12-15)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.15 (2024-12-08)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.14 (2024-12-01)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated pre-commit [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.13 (2024-11-24)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.12 (2024-11-17)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.11 (2024-11-10)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.10 (2024-11-03)
--------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated pre-commit [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.9 (2024-10-27)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.8 (2024-10-20)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.7 (2024-10-13)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated pre-commit [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.6 (2024-10-06)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.5 (2024-09-29)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated pre-commit [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.4 (2024-09-22)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.3 (2024-09-15)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.2 (2024-09-08)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.57.1 (2024-09-01)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]


1.57.0 (2024-08-29)
-------------------

Changes
~~~~~~~
- changelog [PhotosyntheticBatman]
- updated changelog [daniel.howe@hhu.de]

Fix
~~~
- fix the get_derived_compounds() that cause missassignment of compound
  names [PhotosyntheticBatman]


1.56.8 (2024-08-25)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.56.7 (2024-08-18)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.56.6 (2024-08-11)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.56.5 (2024-08-04)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated pre-commit [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.56.4 (2024-07-28)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.56.3 (2024-07-25)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.56.2 (2024-07-22)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- updated changelog [daniel.howe@hhu.de]


1.56.1 (2024-07-18)
-------------------

Changes
~~~~~~~
- updated dependencies [daniel.howe@hhu.de]
- use numpy version 2.0 [Marvin van Aalst]


1.55.1 (2024-06-24)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.55.0 (2024-06-18)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]
- updated poetry lockfile [Marvin van Aalst]


1.54.0 (2024-06-18)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.53.4 (2024-06-17)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.53.3 (2024-06-10)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.53.2 (2024-06-03)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.53.1 (2024-05-31)
-------------------

New
~~~
- get_derived_parameter and get_derived_parameter_value [Marvin van
  Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.52.1 (2024-05-27)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.51.5 (2024-05-20)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.51.4 (2024-05-13)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.51.3 (2024-05-06)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.51.2 (2024-05-03)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.51.1 (2024-04-15)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated poetry lockfile [Marvin van Aalst]


1.51.0 (2024-04-08)
-------------------

New
~~~
- boolean flag to silence warnings in linear label model construction
  [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.50.9 (2024-04-08)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.50.8 (2024-04-01)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.50.7 (2024-03-25)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.50.6 (2024-03-18)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.50.5 (2024-03-11)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.50.4 (2024-02-26)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.50.3 (2024-02-19)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.50.2 (2024-02-12)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.50.1 (2024-02-05)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.50.0 (2024-01-29)
-------------------

New
~~~
- update / scale reaction stoichiometry of compound [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]
- updated poetry lockfile [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.49.11 (2024-01-29)
--------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.49.10 (2024-01-15)
--------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.49.9 (2024-01-08)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.49.8 (2024-01-01)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.49.7 (2023-12-25)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.49.6 (2023-12-20)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.49.5 (2023-12-11)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.49.4 (2023-11-13)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.49.3 (2023-11-09)
-------------------

New
~~~
- verbose kwarg for scale_parameter [Marvin van Aalst]

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated pre-commit [Marvin van Aalst]
- updated changelog [Marvin van Aalst]

Fix
~~~
- handling of readouts in add and update [Marvin van Aalst]


1.49.2 (2023-11-06)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.49.1 (2023-10-30)
-------------------

New
~~~
- add_derived_compound method as shortcut for
  add_algebraic_module_from_args with just a single derived compound
  [Marvin van Aalst]

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated poetry lockfile [Marvin van Aalst]


1.49.0 (2023-10-24)
-------------------

New
~~~
- add_derived_compound method as shortcut for
  add_algebraic_module_from_args with just a single derived compound
  [Marvin van Aalst]


1.48.0 (2023-10-24)
-------------------

New
~~~
- readouts to replace algebraic modules if output isn't required during
  simulation [Marvin van Aalst]
- readouts to replace algebraic modules if output isn't required during
  simulation [Marvin van Aalst]
- get_rate_names containing [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.47.2 (2023-10-23)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.47.1 (2023-10-16)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.47.0 (2023-10-12)
-------------------

New
~~~
- get_(full)_results_and_fluxes_df [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]
- updated poetry lockfile [Marvin van Aalst]


1.46.0 (2023-10-12)
-------------------

New
~~~
- return self for all cud functions [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]
- updated poetry lockfile [Marvin van Aalst]


1.45.0 (2023-10-11)
-------------------

New
~~~
- update_parameter(s)_and functions for Simulator to enable method
  chaining [Marvin van Aalst]
- simulate_and method to chain simulations [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]
- updated poetry lockfile [Marvin van Aalst]
- updated changelog [Marvin van Aalst]

Fix
~~~
- derived parameter sorting [Marvin van Aalst]


1.43.7 (2023-10-09)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.44.0 (2023-10-05)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- derived parameter sorting [Marvin van Aalst]


1.43.6 (2023-10-02)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.43.5 (2023-09-18)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.43.4 (2023-09-11)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.43.3 (2023-09-04)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.43.2 (2023-08-28)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.43.1 (2023-08-21)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.43.0 (2023-08-15)
-------------------

New
~~~
- Simulator.initialise now returns reference [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.42.4 (2023-08-14)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.42.3 (2023-08-07)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.42.2 (2023-07-31)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.42.1 (2023-07-24)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.42.0 (2023-07-18)
-------------------

New
~~~
- get and plot right hand side in simulator [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.41.1 (2023-07-17)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.41.0 (2023-07-13)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- sort derived parameters [Marvin van Aalst]
- sort derived parameters [Marvin van Aalst]


1.40.0 (2023-07-11)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- improved handling of derived parameters [Marvin van Aalst]


1.39.1 (2023-07-10)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.39.0 (2023-07-04)
-------------------

New
~~~
- plot production and consumption [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.38.1 (2023-07-03)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.38.0 (2023-06-28)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.37.3 (2023-06-26)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.37.2 (2023-06-19)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.37.1 (2023-06-12)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.37.0 (2023-06-07)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- remove derived stoichiometries upon removing reaction [Marvin van
  Aalst]


1.36.1 (2023-06-05)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated changelog [Marvin van Aalst]

Fix
~~~
- rel norm at all appropriate places [Marvin van Aalst]


1.36.0 (2023-05-30)
-------------------

New
~~~
- allow relative difference in simulate_to_steady_state [Marvin van
  Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.34.4 (2023-05-30)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.34.3 (2023-05-15)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.34.1 (2023-05-08)
-------------------

New
~~~
- added max_workers kwarg to multiprocessing fns [Marvin van Aalst]
- added multiprocessing to response coefficients [Tobias Pfennig]

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.33.4 (2023-05-01)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated changelog [Marvin van Aalst]

Fix
~~~
- included all attributes in copy method [Tobias Pfennig]


1.33.2 (2023-03-14)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.33.1 (2023-03-13)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.33.0 (2023-03-08)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- algebraic modules are now sorted by args [Marvin van Aalst]


1.32.0 (2023-03-06)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- algebraic module sorting for modifiers [Marvin van Aalst]


1.31.2 (2023-03-06)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.31.1 (2023-02-21)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.31.0 (2023-02-21)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- derived stoichiometries now correctly update [Marvin van Aalst]


1.30.0 (2023-02-06)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.29.2 (2023-02-06)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.29.1 (2023-01-30)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.29.0 (2023-01-27)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- get_variable can again take keyword argument [Marvin van Aalst]


1.28.2 (2023-01-23)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.28.1 (2023-01-16)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.28.0 (2023-01-11)
-------------------

New
~~~
- keyword argument to disable tqdm output in mca methods [Marvin van
  Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.27.0 (2023-01-09)
-------------------

New
~~~
- added custom legends for label simulator [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.26.2 (2023-01-09)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.26.1 (2023-01-09)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.26.0 (2023-01-05)
-------------------

New
~~~
- Simulator.get_new_y0 function to extract last concentration [Marvin
  van Aalst]
- mca.plot_multiple by default now uses a neutral midpoint for the
  heatmap [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.25.6 (2023-01-02)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.25.5 (2022-12-27)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.25.4 (2022-12-19)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.25.3 (2022-12-12)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.25.2 (2022-11-29)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.25.1 (2022-11-29)
-------------------

New
~~~
- more places to write meta info [Marvin van Aalst]

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.24.1 (2022-11-21)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.24.0 (2022-11-15)
-------------------

New
~~~
- added legend_prefix keyword to linear label plots [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.23.0 (2022-11-15)
-------------------

New
~~~
- added initial index argument for plotting linear label results [Marvin
  van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.22.3 (2022-11-14)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.22.2 (2022-11-07)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.22.1 (2022-10-31)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.22.0 (2022-10-26)
-------------------

New
~~~
- derived stoichiometries [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.21.1 (2022-10-26)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.21.0 (2022-10-26)
-------------------

New
~~~
- added convenience functions to convert between parameters and
  compounds [Marvin van Aalst]

Fix
~~~
- wrong warnings when adding models together [Marvin van Aalst]


1.20.0 (2022-10-25)
-------------------

New
~~~
- added scale_parameter method [Marvin van Aalst]

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- adding models ignored derived parameters [Marvin van Aalst]


1.19.0 (2022-10-24)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- respecting algebraic module order [Marvin van Aalst]


1.18.11 (2022-10-24)
--------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]


1.18.9 (2022-09-26)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.18.8 (2022-09-19)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.18.7 (2022-09-12)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.18.6 (2022-09-05)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.18.5 (2022-08-29)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.18.4 (2022-08-22)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.18.3 (2022-08-15)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.18.2 (2022-08-08)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.18.1 (2022-08-01)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.17.4 (2022-07-25)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]

Fix
~~~
- disallow linear label reactions with the same influx and outflux
  [Marvin van Aalst]
- linearlabelmodel stoichiometries [Marvin van Aalst]


1.17.1 (2022-07-18)
-------------------

New
~~~
- fitting methods for steady-state and time-series data [Marvin van
  Aalst]

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.14.3 (2022-07-11)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.14.2 (2022-07-08)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.14.1 (2022-06-27)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.14.0 (2022-06-20)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.13.1 (2022-06-20)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.13.0 (2022-06-15)
-------------------

Changes
~~~~~~~
- add_reaction_from_args now automatically assigns reversibility [Marvin
  van Aalst]


1.11.0 (2022-06-14)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.10.1 (2022-06-13)
-------------------

New
~~~
- added algebraic module from args construction routine [Marvin van
  Aalst]

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.9.3 (2022-06-07)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.9.2 (2022-05-30)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.9.0 (2022-05-24)
------------------

Changes
~~~~~~~
- update version number [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.8.4 (2022-05-23)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.8.2 (2022-05-16)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.8.1 (2022-05-09)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.8.0 (2022-05-06)
------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]

Fix
~~~
- fixes #41 [Marvin van Aalst]


1.7.1 (2022-05-02)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.7.0 (2022-04-28)
------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.6.2 (2022-04-25)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.6.1 (2022-04-18)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.5.17 (2022-04-11)
-------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.5.16 (2022-04-11)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.5.15 (2022-03-28)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.5.14 (2022-03-21)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.5.13 (2022-03-14)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.5.12 (2022-03-07)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.5.11 (2022-02-28)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.5.10 (2022-02-21)
-------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.5.9 (2022-02-14)
------------------

Changes
~~~~~~~
- updated changelog [Marvin van Aalst]


1.5.8 (2022-02-14)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated changelog [Marvin van Aalst]


1.5.7 (2022-02-07)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]


1.5.6 (2022-01-31)
------------------

Changes
~~~~~~~
- updated dependencies [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]
- updated dependencies [Marvin van Aalst]


